@extends('layouts.layout')

@section('content')

<ul class="collapsible">
    <li>
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 4</div>
        <div class="collapsible-body"><span><blockquote>Criar algoritmos com um campo que possa receber apenas 
            números e virgulas, separe os valores enviados e valide aqueles que são um número válido da Sequência 
            de Fibonacci e imprima os números de forma crescente, conforme o exemplo:
                <li>CAMPO RECEBE: 1,13,55,21,5,83</li> 
                <li>IMPRIME: 1,5,13,21,55</li></blockquote></span>
        
        <div class="row">
        <form action="" class="col s12" method="post">        
            
                @csrf

                <div class="row">
                    <div class="input-field col s4">
                        <input id="numeros" name="numeros" type="text" pattern="[0-9,]+" class="validate" required title="Siga o formato 1,2,3...">
                        <label for="numeros">Entre com os números</label>
                        <span class="helper-text" data-error="Campo aceita somente número e vírgula" data-success=""></span><br />
                        <div><button class="waves-effect waves-light btn" type="submit">Enviar</button></div>
                    </div>
                </div>
            </div>
        </form>
        </div>
    </li>
</ul>

@endsection
