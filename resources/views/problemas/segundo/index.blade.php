@extends('layouts.layout')

@section('content')

<ul class="collapsible">
    <li>
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 2</div>
        <div class="collapsible-body"><span><blockquote>A biblioteca de uma universidade deseja fazer um algoritmo que 
            leia o nome do livro que será emprestado, o tipo de usuário (professor ou aluno), o algoritmo 
            deve imprimir um recibo mostrando o nome do livro, o tipo de usuário por extenso e o total de 
            dias de empréstimo. <em>Considerar que o professor tem 10 dias para devolver o livro o aluno somente 3 dias.</em></blockquote></span>
        
            <div class="row">
            <form action="" class="col s12" method="post" target="_blank">

                @csrf

                <div class="row">
                    <div class="input-field col s4">
                        <input id="nome-livro" name="nome-livro" type="text" required class="validate">
                        <label for="nome-livro">Nome do livro</label>
                        <span class="helper-text" data-error="Campo nome aceita somente caracteres" data-success=""></span>
                    </div>

                    <div class="input-field col s4">
                        <select name="tipo-usuario" required class="validate">
                            <option value="" disabled selected>Escolha uma opção</option>
                            <option value="Aluno">Aluno</option>
                            <option value="Professor">Professor</option>
                        </select>
                        <label>Tipo de usuário</label>
                    </div>
                   
                </div>
                <div><button type="submit" class="waves-effect waves-light btn">Imprimir recibo</button></div>
            </form>
            </div>
        
        
        </div> 
    </li>
</ul>

@endsection