<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Recibo </title>
</head>
<body onload="window.print()">
    
    <h5>Recibo de empréstimo de livro</h5>
    <p>
        <strong>Data do empréstimo:</strong> {{ $dataEmprestimo }}<br />
        <strong>Nome do livro: </strong>{{ ( is_object($livro) ? $livro[0]->nome_livro.' - '.$livro[0]->autor : $livro) }}<br />
        <strong>Tipo de usuário: </strong>{{ ( $tipoUsuario ) }}<br />
        <strong>Total de dias de empréstimo: </strong>{{ $totalDias }}<br />
        <strong>Data da devolução:</strong> {{ $diaDevolucao }}
    </p>

</body>
</html>