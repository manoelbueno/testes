@extends('layouts.layout')

@section('content')

<ul class="collapsible">
    <li>
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 1</div>
        <div class="collapsible-body"><span><blockquote>Chico tem 1,50m e cresce 2 centímetros por ano, enquanto Juca tem 1,10m e cresce 3 
          centímetros por ano. Construir um algoritmo que calcule e imprima quantos anos serão 
          necessários para que Juca seja maior que Chico.</blockquote></span>

          <a class="waves-effect waves-light btn" href="{!! url('primeiro/resultado') !!}">Calcular</a>
     
      </div>    
    </li>
</ul>

@endsection