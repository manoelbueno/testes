@extends('layouts.layout') 

@section('content')
<ul class="collapsible">
    <li class="active">
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 1</div>
        <div class="collapsible-body">
                <p>Serão necessários {!! $anos !!} anos!</p>
        </div>
    </li>
</ul>

@endsection