@extends('layouts.layout')

@section('content')

<ul class="collapsible">
    <li>
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 3</div>
        <div class="collapsible-body"><span>Criar um algoritmo com uma matriz 5x5 e imprima: toda a matriz, 
            a matriz gerada só com números ímpares e outra só com números pares.</span>
            
        <div><a href="{!! url('terceiro/resultado') !!}" class="waves-effect waves-light btn">Imprimir Matrizes</a></div>

        </div>
    </li>
</ul>

@endsection