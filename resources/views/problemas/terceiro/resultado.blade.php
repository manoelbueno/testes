@extends('layouts.layout')

@section('content')

<ul class="collapsible">
    <li class="active">
        <div class="collapsible-header"><i class="material-icons">arrow_drop_down_circle</i>Problema 3</div>
        <div class="collapsible-body"><span>Criar um algoritmo com uma matriz 5x5 e imprima: toda a matriz, 
            a matriz gerada só com números ímpares e outra só com números pares.</span>
        </div>

        <div class="col s6">
        <div class="row">
        <div class="col s2">
            <h5>Impressão toda a matriz</h5>
            <table cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="400">
                    <tr>
                        <td height="19" width="20%">{{ $matriz[0][0] }}</td>
                        <td height="19" width="20%">{{ $matriz[0][1] }}</td>
                        <td height="19" width="20%">{{ $matriz[0][2] }}</td>
                        <td height="19" width="20%">{{ $matriz[0][3] }}</td>
                        <td height="19" width="20%">{{ $matriz[0][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matriz[1][0] }}</td>
                        <td height="19" width="20%">{{ $matriz[1][1] }}</td>
                        <td height="19" width="20%">{{ $matriz[1][2] }}</td>
                        <td height="19" width="20%">{{ $matriz[1][3] }}</td>
                        <td height="19" width="20%">{{ $matriz[1][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matriz[2][0] }}</td>
                        <td height="19" width="20%">{{ $matriz[2][1] }}</td>
                        <td height="19" width="20%">{{ $matriz[2][2] }}</td>
                        <td height="19" width="20%">{{ $matriz[2][3] }}</td>
                        <td height="19" width="20%">{{ $matriz[2][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matriz[3][0] }}</td>
                        <td height="19" width="20%">{{ $matriz[3][1] }}</td>
                        <td height="19" width="20%">{{ $matriz[3][2] }}</td>
                        <td height="19" width="20%">{{ $matriz[3][3] }}</td>
                        <td height="19" width="20%">{{ $matriz[3][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matriz[4][0] }}</td>
                        <td height="19" width="20%">{{ $matriz[4][1] }}</td>
                        <td height="19" width="20%">{{ $matriz[4][2] }}</td>
                        <td height="19" width="20%">{{ $matriz[4][3] }}</td>
                        <td height="19" width="20%">{{ $matriz[4][4] }}</td>
                    </tr>
            </table>
        </div>

        <div class="col s2">
            <h5>Impressão matriz pares</h5>
            <table cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="400">
                    <tr>
                        <td height="19" width="20%">{{ $matrizPares[0][0] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[0][1] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[0][2] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[0][3] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[0][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizPares[1][0] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[1][1] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[1][2] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[1][3] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[1][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizPares[2][0] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[2][1] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[2][2] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[2][3] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[2][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizPares[3][0] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[3][1] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[3][2] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[3][3] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[3][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizPares[4][0] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[4][1] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[4][2] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[4][3] }}</td>
                        <td height="19" width="20%">{{ $matrizPares[4][4] }}</td>
                    </tr>
            </table>
        </div>

        <div class="col s2">
            <h5>Impressão matriz ímpares</h5>
            <table cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="400">
                    <tr>
                        <td height="19" width="20%">{{ $matrizImpares[0][0] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[0][1] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[0][2] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[0][3] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[0][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizImpares[1][0] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[1][1] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[1][2] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[1][3] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[1][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizImpares[2][0] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[2][1] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[2][2] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[2][3] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[2][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizImpares[3][0] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[3][1] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[3][2] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[3][3] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[3][4] }}</td>
                    </tr>
                    <tr>
                        <td height="19" width="20%">{{ $matrizImpares[4][0] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[4][1] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[4][2] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[4][3] }}</td>
                        <td height="19" width="20%">{{ $matrizImpares[4][4] }}</td>
                    </tr>
            </table>
        </div>

        </div>
        </div>
    </li>
</ul>

@endsection