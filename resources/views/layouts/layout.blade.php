<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <title>{{ $title }}</title>
</head>
<body>

    <nav>
        <div class="nav-wrapper grey darken-4">
            <ul id="nav-mobile" class="left hide-on-med-and-down">
                <li><a href=" {{ url('/') }} ">Home</a></li>
            <li><a href=" {{ url('primeiro') }} ">Problema 1</a></li>
            <li><a href=" {{ url('segundo') }} ">Problema 2</a></li>
            <li><a href=" {{ url('terceiro') }}">Problema 3</a></li>
            <li><a href=" {{ url('quarto') }}">Problema 4</a></li>
            </ul>
        </div>
    </nav>
    
    <main>
        @yield('content')
    </main>

    <footer class="page-footer grey darken-4">
        <div class="container">
            <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Tecnologias Utilizadas</h5>
                <ul>
                <li>Apache 2.4.29 (Ubuntu)</li>
                <li>Laravel 5.7</li>
                <li>MaterializeCSS 1.0.0</li>
                <li>MySQL 8.0</li>
                <li>MySQL Workbench</li>
                <li>POO</li>
                </ul>
                <h5 class="white-text">Git Repository Utilizado</h5>
                <a class="grey-text text-lighten-3" href="https://bitbucket.org/manoelbueno/testes/src/master/" target="_blank">https://bitbucket.org/manoelbueno/testes/src/master/</a>
            </div>

            <div class="footer-copyright">
                <div class="container">   
                <a class="grey-text text-lighten-4 right" href="https://www.appfacilita.com/"><img src="https://www.appfacilita.com/wp-content/themes/site-facilita/_assets/_imgs/facilita-branco.png" alt="facilita"></a>
                </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src=" {{ url('js/script.js') }}"></script>

</body>
</html>