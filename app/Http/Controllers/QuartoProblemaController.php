<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QuartoProblemaController extends Controller
{
    public function quartoProblema() {

        return view('problemas.quarto.index',
            array(
                'title' => 'P4 - Manoel Bueno'
            )
        );
    }

    public function processarNumeros(Request $request) {

        $numeros = $request->input('numeros');

        if( $numeros == '' ) {
            return redirect('quarto');
        } else {
                
            $arr_numeros = explode(",", $numeros);
            sort($arr_numeros);
            $maior = end($arr_numeros);

            $arr_fibonacci = $this->fibonacci($maior);
            
            $resultado = '';

            $i = 0;
            $arr_size = count($arr_numeros);
            for($i = 0; $i < $arr_size - 1; $i++) {
                if(in_array($arr_numeros[$i], $arr_fibonacci))
                    $resultado = $resultado.$arr_numeros[$i].',';
            }

            if(in_array(end($arr_numeros), $arr_fibonacci))
                $resultado = $resultado.end($arr_numeros);

            return view('problemas.quarto.resultado',
                array(
                    'title' => 'Resultado P4 - Manoel Bueno',
                    'numeros' => $numeros,
                    'resultado' => $resultado
                )
            );
        }

    }

    public function fibonacci($valor) {
        $arr = array(0, 1);
        $i = 2;
        do {
            array_push($arr, $arr[$i - 1] + $arr[$i - 2]);
            $i++;
        } while($arr[$i-1] < $valor);

        return $arr;
    }
}