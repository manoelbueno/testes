<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Segundo;

class SegundoProblemaController extends Controller
{
    public function segundoProblema() {

        return view('problemas.segundo.index',
            array(
                'title' => 'P 2 - Manoel Bueno'
            )
        );
    }

    public function gerarRecibo(Request $request) {

        $nomeLivro = $request->input('nome-livro');
        $tipoUsuario = $request->input('tipo-usuario');
        
        $totalDias = ( $tipoUsuario == 'Aluno' ? '3' : ($tipoUsuario == 'Professor' ? '10' : ''));
        $diaDevolucao = ( $tipoUsuario == 'Aluno' ? date('d/m/Y' ,strtotime('+3 day')) : ( $tipoUsuario == 'Professor' ? date('d/m/Y' ,strtotime('+10 day')) : ''));

        $livro = Segundo::where('nome_livro','LIKE','%'.$nomeLivro.'%')->orWhere('autor','LIKE','%'.$nomeLivro.'%')->get();
        
        // retorna view com livro do banco de dados
        if($nomeLivro != null && $tipoUsuario != null && count($livro) > 0) {
            return view('problemas.segundo.recibo',
                array(
                    'title' => 'Resultado P2 - Manoel Bueno',
                    'livro' => $livro,
                    'tipoUsuario' => $tipoUsuario,
                    'dataEmprestimo' => date('d/m/Y'),
                    'totalDias' => $totalDias,
                    'diaDevolucao' => $diaDevolucao
                )
            );
        }
        // retorna view com livro do usuário
        else return view ('problemas.segundo.recibo',
                array(
                    'title' => 'Resultado P2 - Manoel Bueno',
                    'livro' => $nomeLivro,
                    'tipoUsuario' => $tipoUsuario,
                    'dataEmprestimo' => date('d/m/Y'),
                    'totalDias' => $totalDias,
                    'diaDevolucao' => $diaDevolucao
                )
        );
        
    }
}
