<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TerceiroProblemaController extends Controller
{
    public function terceiroProblema() {

        return view('problemas.terceiro.index',
            array(
                'title' => 'P 3 - Manoel Bueno'
                
            )
        );
    }

    public function imprimirMatrizes() {

        $matriz = array(
            array(10, 25, 3, 4, 17),
            array(18, 24, 5, 9, 14),
            array(16, 14, 19, 24, 13),
            array(25, 8, 7, 32, 10),
            array(9, 1, 4, 33, 6)
        );

        $matrizPares = array();
        $matrizImpares = array();

        for($i=0; $i < 5; $i++){

            $matrizPares[$i] = array();
            $matrizImpares[$i] = array();

            for($j=0; $j<5; $j++){
                if($matriz[$i][$j] % 2 == 0) {
                    $matrizPares[$i][$j] = $matriz[$i][$j];
                    $matrizImpares[$i][$j] = '-';
                }
                else {
                    $matrizPares[$i][$j] = '-';
                    $matrizImpares[$i][$j] = $matriz[$i][$j];
                }
            }
        }

        return view('problemas.terceiro.resultado',
        array(
            'title' => 'P3 - Resultado',
            'matriz' => $matriz,
            'matrizPares' => $matrizPares,
            'matrizImpares' => $matrizImpares
        )
    );
        

    }
}
