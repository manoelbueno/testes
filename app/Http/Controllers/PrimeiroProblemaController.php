<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CustomClasses\Pessoa;

class PrimeiroProblemaController extends Controller
{
    public function primeiroProblema() {

        return view('problemas.primeiro.index',
            array(
                'title' => 'P 1 - Manoel Bueno'
            )
        );
    }

    public function calcularAnos() {

        $anos = 0;
        $chico = new Pessoa("Chico", 1.5, 0.02);
        $juca = new Pessoa("Juca", 1.1, 0.03);

        while($juca->getAltura() < $chico->getAltura()) {
            $juca->crescer();
            $chico->crescer();
            $anos++;
        }
       
        return view('problemas.primeiro.resultado',
            array(
                'title' => 'Resultado P1 - Manoel Bueno',
                'anos' => $anos
            )
        );
    }
}
