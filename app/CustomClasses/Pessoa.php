<?php

namespace App\CustomClasses;

class Pessoa
{
    private $_nome;
    private $_altura;
    private $_taxaCrescimento;

    public function __construct($nome, $altura, $taxaCrescimento)
    {
        $this->_nome = $nome;
        $this->_altura = $altura;
        $this->_taxaCrescimento = $taxaCrescimento;
    }

    public function crescer() {
        $this->_altura += $this->_taxaCrescimento;
    }

    public function getAltura() {
        return $this->_altura;
    }
}