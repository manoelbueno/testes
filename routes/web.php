<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('primeiro', 'PrimeiroProblemaController@primeiroProblema');
Route::get('primeiro/resultado', 'PrimeiroProblemaController@calcularAnos');
Route::get('segundo', 'SegundoProblemaController@segundoProblema');
Route::post('segundo', 'SegundoProblemaController@gerarRecibo');
Route::get('terceiro', 'TerceiroProblemaController@terceiroProblema');
Route::get('terceiro/resultado', 'TerceiroProblemaController@imprimirMatrizes');
Route::get('quarto', 'QuartoProblemaController@quartoProblema');
Route::post('quarto', 'QuartoProblemaController@processarNumeros');