DROP TABLE IF EXISTS `livros`;

CREATE TABLE livros (
	id INT NOT NULL AUTO_INCREMENT, 
    nome_livro VARCHAR(100), 
    autor VARCHAR(100),
    primary key (id)
);

INSERT INTO livros ( nome_livro, autor )
   VALUES
   ('Doing Data Science: Straight Talk from the Frontline' , 'Cathy and Rachel Schutt'),
   ('Introduction to Algorithms', 'Thoams H. Cormen'),
   ('Cálculo - Volume 2', 'James Stewart'),
   ('Artificial Intelligence: A Modern Approach', 'Peter Norvig');
   
SELECT * FROM livros;